package v4

import (
	"errors"
	"strconv"
	"strings"
)



type IpRange struct {
	start *IpAddress
	end   *IpAddress
	mask  int
}

func NewIpRange(str string) (*IpRange, error) {
	tmp := new(IpRange)
	err := tmp.Assign(str)
	return tmp, err
}

func (a *IpRange) Assign(v string) error {
	ipSegs := strings.Split(v, IpRangeConnector)
	switch len(ipSegs) {
	case 1:
		return a.translateIpMask(v)
	case 2:
		start, err := NewIpAddress(ipSegs[0])
		if err != nil {
			return err
		}
		end, err := NewIpAddress(ipSegs[0])
		if err != nil {
			return err
		}
		a.start = start
		a.end = end
	default:
		return errors.New("unrecognized input string pattern")
	}
	if a.start.num > a.end.num {
		a.start, a.end = a.end, a.start
	}
	return nil
}

func (a *IpRange) translateIpMask(v string) error {
	ipSegs := strings.Split(v, IpRangeMask)
	if len(ipSegs) == 1 {
		start, err := NewIpAddress(ipSegs[0])
		if err != nil {
			return err
		}
		a.start = start
		a.end = start
		a.mask = bitLength32
		return nil
	}
	if len(ipSegs) == 2 {
		start, err := NewIpAddress(ipSegs[0])
		if err != nil {
			return err
		}
		bits, err := strconv.Atoi(ipSegs[1])
		if err != nil {
			return err
		}
		a.mask = bits
		bits = bitLength32 - bits
		increment := 1 << bits
		end := &IpAddress{num: start.num + increment - 1}
		end.str = end.String()
		a.start = start
		a.end = end
		return nil
	}
	return errors.New("unrecognized input string pattern")
}

func (a *IpRange) String() string {
	if a.start.num == a.end.num {
		return a.start.str
	}
	// if a.mask
	return strings.Join([]string{a.start.str, a.end.str}, IpRangeConnector)
}

func (a *IpRange) Equal(b *IpRange) bool {
	if (a.start.num == b.start.num) && (a.end.num == b.end.num) {
		return true
	}
	return false
}
