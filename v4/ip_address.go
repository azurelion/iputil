package v4

import (
	"errors"
	"strconv"
	"strings"
)



type IpAddress struct {
	num uint32
	str string
}

func NewIpAddress(str string) (*IpAddress, error) {
	tmp := new(IpAddress)
	err := tmp.Assign(str)
	return tmp, err
}

func (a *IpAddress) Assign(v string) error {
	ipSegs := strings.Split(v, ".")
	if len(ipSegs) != lenV4 {
		return errors.New("invalid input length")
	}
	var ipInt uint32 = 0
	var pos uint = 24
	for _, ipSeg := range ipSegs {
		tempInt, err := strconv.Atoi(ipSeg)
		if err != nil {
			return err
		}
		tempInt = tempInt << pos
		ipInt = ipInt | uint32(tempInt)
		pos -= 8
	}
	a.num = ipInt
	a.str = a.String()
	return nil
}

func (a *IpAddress) String() string {
	ipInt := a.num
	ipSegs := make([]string, lenV4)
	for i := 0; i < lenV4; i++ {
		tempInt := int(ipInt & 0xFF)
		ipSegs[lenV4-i-1] = strconv.Itoa(tempInt)
		ipInt = ipInt >> 8
	}
	return strings.Join(ipSegs, ".")
}

func (a *IpAddress) Equal(b *IpAddress) bool {
	return a.num == b.num
}

func (a *IpAddress) Compare(b *IpAddress) int {
	if a.num > b.num {return 1}
	if a.num < b.num {return -1}
	return 0
}