package v4

var IpRangeConnector = "-"
var IpRangeMask = "/"
var lenV4 = 4
var bitLength32 = 32

type ipUtil interface {
	Assign(string) error
	String() string
	Add() (IpSet,error)
}