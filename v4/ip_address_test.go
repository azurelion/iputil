package v4

import "testing"

type IpAddressTest struct {
	in  uint32
	out string
}

var ipAddressTests = []IpAddressTest{
	{2130706433, "127.0.0.1"},
	{65535, "0.0.255.255"},
	{256, "0.0.1.0"},
	{8, "0.0.0.8"},
	{0, "0.0.0.0"},
}

func TestIpAddressString(t *testing.T) {
	for _, test := range ipAddressTests {
		s := (&IpAddress{num: test.in}).String()
		if s != test.out {
			t.Errorf("IpAddressString(%v) = %v want %v",
				test.in, s, test.out)
		}
		ia, _ := NewIpAddress(test.out)
		if ia.num != test.in {
			t.Errorf("IpAddressAssign(%v) = %v want %v",
				test.out, ia.num, test.in)
		}
	}
}
